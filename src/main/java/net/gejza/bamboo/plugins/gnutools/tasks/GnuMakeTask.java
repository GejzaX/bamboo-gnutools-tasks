package net.gejza.bamboo.plugins.gnutools.tasks;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.process.CommandlineStringUtils;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.*;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Created by gejza on 4.5.14.
 */
public class GnuMakeTask implements TaskType {
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String GNUMAKE_CAPABILITY_PREFIX = CapabilityDefaultsHelper.CAPABILITY_BUILDER_PREFIX  + ".make";
    public static final String GNUMAKE_EXE_NAME = "make";
    public static final String GNUMAKE_LABEL = "GNU Make";
    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    private final ProcessService processService;
    private final EnvironmentVariableAccessor environmentVariableAccessor;
    private final CapabilityContext capabilityContext;
    // ---------------------------------------------------------------------------------------------------- Constructors

    public GnuMakeTask(final ProcessService processService, final EnvironmentVariableAccessor environmentVariableAccessor, final CapabilityContext capabilityContext) {
        this.processService = processService;
        this.environmentVariableAccessor = environmentVariableAccessor;
        this.capabilityContext = capabilityContext;
    }

    // ----------------------------------------------------------------------------------------------- Interface Methods
    @NotNull
    @Override
    public TaskResult execute(final TaskContext taskContext) throws TaskException {
        final TaskResultBuilder taskResultBuilder = TaskResultBuilder.create(taskContext);

        final BuildLogger buildLogger = taskContext.getBuildLogger();
        try {
            final ConfigurationMap configurationMap = taskContext.getConfigurationMap();
            final String makePath = Preconditions.checkNotNull(
                    capabilityContext.getCapabilityValue(
                            GnuMakeTask.GNUMAKE_CAPABILITY_PREFIX + "." + configurationMap.get(AbstractGnuToolsConfigurator.RUNTIME)),
                    "Make path is not defined");
            //final String command = configurationMap.get(AbstractGnuToolsConfigurator.COMMAND);
            final String target = configurationMap.get(GnuMakeTaskConfigurator.TARGET);
            final Map<String, String> extraEnvironmentVariables = environmentVariableAccessor.splitEnvironmentAssignments(configurationMap.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES), false);

            final List<String> commandsList = Lists.newArrayList(makePath);
            //commandsList.addAll(CommandlineStringUtils.tokeniseCommandline(command));

            if (StringUtils.isNotBlank(target)) {
                commandsList.addAll(CommandlineStringUtils.tokeniseCommandline(target));
            }

            taskResultBuilder.checkReturnCode(processService.executeProcess(taskContext, new ExternalProcessBuilder()
                    .command(commandsList)
                    .env(extraEnvironmentVariables)
                    .workingDirectory(taskContext.getWorkingDirectory())));

            return taskResultBuilder.build();
        } catch (Exception e) {
            throw new TaskException("Failed to execute task", e);
        }

    }
}
